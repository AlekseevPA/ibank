package com.ibank.security;

/**
 * Created by worker on 11/8/17.
 */
public class SecurityValues {
    public static final String SECRET = "WR6SWa@aPr7k";
    public static final long EXPIRATION_TIME = 60 * 60 * 1000; // 1 hour
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
