package com.ibank.controller;

import com.ibank.entity.BankDeposit;
import com.ibank.service.BankDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by worker on 11/8/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/bank/deposits")
public class BankDepositController {
    private final BankDepositService bankDepositService;

    @Autowired
    public BankDepositController(BankDepositService bankDepositService) {
        this.bankDepositService = bankDepositService;
    }

    @GetMapping
    public List<BankDeposit> getAll(Principal principal) {
        return bankDepositService.findAll();
    }
}
