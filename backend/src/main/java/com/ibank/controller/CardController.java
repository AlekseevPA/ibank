package com.ibank.controller;

import com.ibank.entity.Card;
import com.ibank.entity.Event;
import com.ibank.helper.SecurityHelper;
import com.ibank.helper.TimestampHelper;
import com.ibank.model.CardRequest;
import com.ibank.service.CardService;
import com.ibank.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Created by worker on 10/31/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/cards")
public class CardController {
    private final CardService cardService;
    private final EventService eventService;

    @Autowired
    public CardController(CardService cardService, EventService eventService) {
        this.cardService = cardService;
        this.eventService = eventService;
    }

    @GetMapping
    public List<Card> getAll(Principal principal) {
        return cardService.findAll(SecurityHelper.userId(principal));
    }

    @GetMapping("/{id}")
    public Card get(Principal principal, @PathVariable int id) {
        return cardService.findById(SecurityHelper.userId(principal), id);
    }

    @PostMapping
    public void add(Principal principal, @RequestBody CardRequest cardRequest) {
        cardService.create(SecurityHelper.userId(principal), cardRequest.getManufacturer(), cardRequest.getType());
    }

    @DeleteMapping("/{id}")
    public void delete(Principal principal, @PathVariable int id) {
        cardService.block(SecurityHelper.userId(principal), id);
    }
}
