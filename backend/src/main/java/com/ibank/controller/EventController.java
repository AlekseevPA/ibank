package com.ibank.controller;

import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import com.ibank.helper.SecurityHelper;
import com.ibank.repository.EventRepository;
import com.ibank.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by worker on 10/31/17.
 */

@CrossOrigin
@RestController
@RequestMapping("/api/events")
public class EventController {
    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public List<Event> getAll(Principal principal, @RequestParam(defaultValue = "0") int count) {
        return count == 0 ? eventService.getAll(SecurityHelper.userId(principal)) : eventService.getLimited(SecurityHelper.userId(principal), count);
    }

    @GetMapping(params = "page")
    public List<Event> getByPage(Principal principal, @RequestParam int page) {
        return eventService.getPage(SecurityHelper.userId(principal), page);
    }

    @GetMapping("/product/{type}/{id}")
    public List<Event> getForProduct(Principal principal, @PathVariable String type, @PathVariable int id) {
        return eventService.getForProduct(SecurityHelper.userId(principal), id, EventType.valueOf(type));
    }
}
