package com.ibank.controller;

import com.ibank.helper.SecurityHelper;
import com.ibank.model.TransferRequest;
import com.ibank.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by worker on 11/8/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/transfers")
public class TransferController {
    private final TransferService transferService;

    @Autowired
    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping
    public void add(Principal principal, @RequestBody TransferRequest transferRequest) {
        transferService.create(SecurityHelper.userId(principal), transferRequest.getCardId(), transferRequest.getNumber(), transferRequest.getAmount());
    }
}
