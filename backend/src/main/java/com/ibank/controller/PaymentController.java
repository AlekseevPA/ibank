package com.ibank.controller;

import com.ibank.helper.SecurityHelper;
import com.ibank.model.PaymentCommunalServicesRequest;
import com.ibank.model.PaymentFinesRequest;
import com.ibank.model.PaymentMobileRequest;
import com.ibank.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by worker on 11/8/17.
 */

@CrossOrigin
@RestController
@RequestMapping("/api/payments")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/mobile")
    public void mobile(Principal principal, @RequestBody PaymentMobileRequest paymentMobileRequest) {
        paymentService.payForMobile(
                SecurityHelper.userId(principal),
                paymentMobileRequest.getCardId(),
                paymentMobileRequest.getPhone(),
                paymentMobileRequest.getAmount()
        );
    }

    @PostMapping("/communal-services")
    public void communalServices(Principal principal, @RequestBody PaymentCommunalServicesRequest paymentCommunalServicesRequest) {
        paymentService.payForCommunalServices(
                SecurityHelper.userId(principal),
                paymentCommunalServicesRequest.getCardId(),
                paymentCommunalServicesRequest.getOrg(),
                paymentCommunalServicesRequest.getAccount(),
                paymentCommunalServicesRequest.getValue(),
                paymentCommunalServicesRequest.getAmount()
        );
    }

    @PostMapping("/fines")
    public void fines(Principal principal, @RequestBody PaymentFinesRequest paymentFinesRequest) {
        paymentService.payFines(
                SecurityHelper.userId(principal),
                paymentFinesRequest.getCardId(),
                paymentFinesRequest.getCode(),
                paymentFinesRequest.getAmount()
        );
    }
}
