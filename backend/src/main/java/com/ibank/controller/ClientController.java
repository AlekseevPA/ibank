package com.ibank.controller;

import com.ibank.helper.SecurityHelper;
import com.ibank.model.LoginRequest;
import com.ibank.model.PasswordRequest;
import com.ibank.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by worker on 11/1/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/clients")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PutMapping("/login")
    public void changeLogin(Principal principal, @RequestBody LoginRequest loginRequest) {
        clientService.changeLogin(SecurityHelper.userId(principal), loginRequest.getLogin());
    }

    @PutMapping("/password")
    public void changeLogin(Principal principal, @RequestBody PasswordRequest passwordRequest) {
        clientService.changePassword(SecurityHelper.userId(principal), passwordRequest.getCurrent(), passwordRequest.getFuture());
    }
}
