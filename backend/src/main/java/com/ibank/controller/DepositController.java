package com.ibank.controller;

import com.ibank.entity.Deposit;
import com.ibank.helper.SecurityHelper;
import com.ibank.model.DepositCloseRequest;
import com.ibank.model.DepositRefillRequest;
import com.ibank.model.DepositRequest;
import com.ibank.repository.DepositRepository;
import com.ibank.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by worker on 10/31/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/deposits")
public class DepositController {
    private final DepositService depositService;

    @Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @GetMapping
    public List<Deposit> getAll(Principal principal) {
        return depositService.findAll(SecurityHelper.userId(principal));
    }

    @GetMapping("/{id}")
    public Deposit get(Principal principal, @PathVariable int id) {
        return depositService.findById(SecurityHelper.userId(principal), id);
    }

    @PostMapping
    public void add(Principal principal, @RequestBody DepositRequest depositRequest) {
        depositService.create(SecurityHelper.userId(principal), depositRequest.getBankDepositId());
    }

    @PutMapping("/{id}/refill")
    public void add(Principal principal, @PathVariable int id, @RequestBody DepositRefillRequest depositRefillRequest) {
        depositService.refill(SecurityHelper.userId(principal), id, depositRefillRequest.getCardId(), depositRefillRequest.getAmount());
    }

    @PutMapping("/{id}/close")
    public void add(Principal principal, @PathVariable int id, @RequestBody DepositCloseRequest depositCloseRequest) {
        depositService.close(SecurityHelper.userId(principal), id, depositCloseRequest.getCardId());
    }
}
