package com.ibank.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by worker on 10/31/17.
 */
@Entity
@Table(name = "payments")
public class Payment {
    @Id
    @GeneratedValue
    private int id;
    private String name;

    public Payment(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
