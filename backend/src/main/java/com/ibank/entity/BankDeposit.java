package com.ibank.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by worker on 11/8/17.
 */

@Entity
@Table(name = "bank_deposits")
public class BankDeposit {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int duration;
    private int percent;

    public BankDeposit() {
    }

    public BankDeposit(String name, int duration, int percent) {
        this.name = name;
        this.duration = duration;
        this.percent = percent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
