package com.ibank.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by worker on 10/31/17.
 */
@Entity
@Table(name = "deposits")
public class Deposit extends Product {
    private int balance;
    private int percent;
    private int duration;
    private int expire;
    private int timestamp;

    public Deposit() {
    }


    public Deposit(int userId, String name, int balance, int percent, int duration, int timestamp) {
        super(userId, name);
        this.balance = balance;
        this.percent = percent;
        this.duration = duration;
        this.expire = timestamp + duration * 30 * 24 * 3600;
        this.timestamp = timestamp;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
