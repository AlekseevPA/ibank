package com.ibank.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by worker on 11/8/17.
 */
@Entity
@Table(name = "transfers")
public class Transfer {
    @Id
    @GeneratedValue
    private int id;
    private int userId;
    private String from;
    private String to;
    private int timestamp;

    public Transfer() {
    }

    public Transfer(int userId, String from, String to, int timestamp) {
        this.userId = userId;
        this.from = from;
        this.to = to;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
