package com.ibank.entity;

import com.ibank.enumeration.EventType;

import javax.persistence.*;

/**
 * Created by worker on 10/31/17.
 */
@Entity
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue
    private int id;
    private int userId;
    private int productId;
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private EventType type;
    private int amount;
    private int timestamp;

    public Event() {
    }

    public Event(int userId, int productId, String name, String description, EventType type, int timestamp) {
        this(userId, productId, name, description, type, 0, timestamp);
    }

    public Event(int userId, int productId, String name, String description, EventType type, int amount, int timestamp) {
        this.userId = userId;
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.type = type;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
