package com.ibank.entity;

import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;

import javax.persistence.*;

/**
 * Created by worker on 10/31/17.
 */

@Entity
@Table(name = "cards")
public class Card extends Product {
    private String number;
    private String masked;
    private int expire;
    private int balance;
    @Enumerated(EnumType.STRING)
    private CardManufacturer manufacturer;
    @Enumerated(EnumType.STRING)
    private CardType type;
    private boolean blocked;

    public Card() {
    }

    public Card(int userId, String name, String number, String masked, int expire, int balance, CardManufacturer manufacturer, CardType type, boolean blocked) {
        super(userId, name);
        this.number = number;
        this.masked = masked;
        this.expire = expire;
        this.balance = balance;
        this.manufacturer = manufacturer;
        this.type = type;
        this.blocked = blocked;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMasked() {
        return masked;
    }

    public void setMasked(String masked) {
        this.masked = masked;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public CardManufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(CardManufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
