package com.ibank.helper;

/**
 * Created by worker on 11/1/17.
 */
public class TimestampHelper {
    public static int fromNow(int year, int month, int day, int hours, int minutes, int seconds) {
        int now = now();
        return now
                + year * 365 * 24 * 3600
                + month * 30 * 24 * 3600
                + day * 24 * 3600
                + hours * 3600
                + minutes * 60
                + seconds;
    }

    public static int beforeNow(int year, int month, int day, int hours, int minutes, int seconds) {
        int now = now();
        return now
                - year * 365 * 24 * 3600
                - month * 30 * 24 * 3600
                - day * 24 * 3600
                - hours * 3600
                - minutes * 60
                - seconds;
    }

    public static int now() {
        return  (int) (System.currentTimeMillis() / 1000);
    }
}
