package com.ibank.helper;

import java.security.Principal;

/**
 * Created by worker on 11/8/17.
 */
public class SecurityHelper {
    public static int userId(Principal principal) {
        return Integer.valueOf(principal.getName(), 10);
    }
}
