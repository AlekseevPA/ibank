package com.ibank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by worker on 11/1/17.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CommonErrorException extends RuntimeException {
    public CommonErrorException() {
    }

    public CommonErrorException(String message) {
        super(message);
    }
}
