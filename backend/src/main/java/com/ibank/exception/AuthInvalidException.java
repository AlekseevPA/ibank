package com.ibank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by worker on 10/31/17.
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AuthInvalidException extends RuntimeException {
    public AuthInvalidException() {
    }

    public AuthInvalidException(String message) {
        super(message);
    }
}
