package com.ibank.service;

import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import com.ibank.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by worker on 11/1/17.
 */
@Service
public class EventService {
    private final EventRepository eventRepository;
    private static final int limit = 50;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> getAll(int userId) {
        return eventRepository.findAllByUserIdOrderByIdDesc(userId);
    }

    public List<Event> getLimited(int userId, int count) {
        if (count > limit) {
            count = limit;
        }
        return eventRepository.findAllByUserIdOrderByIdDesc(userId, new PageRequest(0, count));
    }

    public List<Event> getPage(int userId, int page) {
        return eventRepository.findAllByUserIdOrderByIdDesc(userId, new PageRequest(page - 1, limit));
    }

    public List<Event> getForProduct(int userId, int productId, EventType type) {
        return eventRepository.findAllByUserIdAndProductIdAndTypeOrderByIdDesc(userId, productId, type);
    }

    public Event save(Event event) {
        return eventRepository.save(event);
    }
}
