package com.ibank.service;

import com.ibank.entity.Client;
import com.ibank.model.AuthRequest;
import com.ibank.model.User;
import com.ibank.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * Created by worker on 10/31/17.
 */
@Service
public class AuthService implements UserDetailsService {
    private final ClientRepository clientRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public AuthService(ClientRepository clientRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.clientRepository = clientRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public Client auth(AuthRequest authRequest) {
        Client client = clientRepository.findByLogin(authRequest.getLogin());
        if (client == null) {
            return null;
        }

        if (!bCryptPasswordEncoder.matches(authRequest.getPassword(), client.getPassword())) {
            return null;
        }

        return client;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            throw new UsernameNotFoundException(login);
        }

        return new User(client.getId(), client.getLogin(), client.getPassword());
    }
}
