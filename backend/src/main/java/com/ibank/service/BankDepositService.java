package com.ibank.service;

import com.ibank.entity.BankDeposit;
import com.ibank.repository.BankDepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by worker on 11/8/17.
 */

@Service
public class BankDepositService {
    private final BankDepositRepository bankDepositRepository;

    @Autowired
    public BankDepositService(BankDepositRepository bankDepositRepository) {
        this.bankDepositRepository = bankDepositRepository;
    }

    public List<BankDeposit> findAll() {
        return bankDepositRepository.findAll();
    }
}
