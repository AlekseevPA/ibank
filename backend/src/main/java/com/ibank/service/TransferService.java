package com.ibank.service;

import com.ibank.entity.Card;
import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import com.ibank.exception.CommonErrorException;
import com.ibank.helper.TimestampHelper;
import com.ibank.repository.CardRepository;
import com.ibank.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by worker on 11/8/17.
 */
@Service
public class TransferService {
    private final TransferRepository transferRepository;
    private final CardRepository cardRepository;
    private final EventService eventService;

    @Autowired
    public TransferService(
            TransferRepository transferRepository,
            CardRepository cardRepository,
            EventService eventService
    ) {
        this.transferRepository = transferRepository;
        this.cardRepository = cardRepository;
        this.eventService = eventService;
    }

    @Transactional
    public void create(int userId, int cardId, String number, int amount) {
        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        if (card == null) {
            throw new CommonErrorException("Карта не найдена");
        }

        if (amount < 0) {
            throw new CommonErrorException("Неверная сумма");
        }

        if (amount > card.getBalance()) {
            throw new CommonErrorException("Недостаточно средств");
        }

        if (card.getNumber().equals(number)) {
            throw new CommonErrorException("Указана та же карта");
        }

        card.setBalance(card.getBalance() - amount);
        eventService.save(new Event(userId, card.getId(), "Перевод средств", "", EventType.CARD, -amount, TimestampHelper.now()));

        Card target = cardRepository.findByNumber(number);
        if (target != null) {
            target.setBalance(target.getBalance() + amount);
            eventService.save(new Event(target.getUserId(), target.getId(), "Перевод средств", "", EventType.CARD, amount, TimestampHelper.now()));
        }

    }
}
