package com.ibank.service;

import com.ibank.entity.Card;
import com.ibank.entity.Event;
import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;
import com.ibank.enumeration.EventType;
import com.ibank.exception.CommonErrorException;
import com.ibank.helper.TimestampHelper;
import com.ibank.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by worker on 11/1/17.
 */
@Service
public class CardService {
    private final CardRepository cardRepository;
    private final EventService eventService;

    @Autowired
    public CardService(CardRepository cardRepository, EventService eventService) {
        this.cardRepository = cardRepository;
        this.eventService = eventService;
    }

    public List<Card> findAll(int userId) {
        return cardRepository.findAllByUserId(userId);
    }

    public Card findById(int userId, int productId) {
        return cardRepository.findByUserIdAndId(userId, productId);
    }

    @Transactional
    public void create(int userId, CardManufacturer manufacturer, CardType type) {
        String number = (manufacturer == CardManufacturer.MASTERCARD ? "52" : "46") + String.valueOf(System.currentTimeMillis());
        Card card = cardRepository.save(
                new Card(
                        userId,
                        "",
                        number,
                        "*" + number.substring(11),
                        TimestampHelper.fromNow(2, 0, 0, 0,0, 0),
                        0,
                        manufacturer,
                        type,
                        false
                )
        );
        eventService.save(new Event(userId, card.getId(), "Открытие карты", "", EventType.CARD, TimestampHelper.now()));
    }

    @Transactional
    public void block(int userId, int productId) {
        Card card = cardRepository.findByUserIdAndId(userId, productId);
        if (card == null) {
            throw new CommonErrorException("Карта не найдена");
        }
        card.setBlocked(true);
        cardRepository.save(card);
        eventService.save(new Event(userId, card.getId(), "Блокировка карты: " + card.getMasked(), "", EventType.CARD, TimestampHelper.now()));
    }

    public Card save(Card card) {
        return cardRepository.save(card);
    }
}
