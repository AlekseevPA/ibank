package com.ibank.service;

import com.ibank.entity.Card;
import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import com.ibank.exception.CommonErrorException;
import com.ibank.helper.TimestampHelper;
import com.ibank.repository.CardRepository;
import com.ibank.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by worker on 11/8/17.
 */

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final CardRepository cardRepository;
    private final EventService eventService;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository, CardRepository cardRepository, EventService eventService) {
        this.paymentRepository = paymentRepository;
        this.cardRepository = cardRepository;
        this.eventService = eventService;
    }

    @Transactional
    public void payForMobile(int userId, int cardId, String phone, int amount) {
        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        validateCardAndAmount(card, amount);

        card.setBalance(card.getBalance() - amount);
        eventService.save(new Event(userId, card.getId(), "Оплата моб.телефона", "", EventType.CARD, -amount, TimestampHelper.now()));
    }

    @Transactional
    public void payForCommunalServices(int userId, int cardId, String org, String account, String value, int amount) {
        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        validateCardAndAmount(card, amount);

        card.setBalance(card.getBalance() - amount);
        eventService.save(new Event(userId, card.getId(), "Оплата ЖКУ", "", EventType.CARD, -amount, TimestampHelper.now()));
    }

    @Transactional
    public void payFines(int userId, int cardId, String code, int amount) {
        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        validateCardAndAmount(card, amount);

        card.setBalance(card.getBalance() - amount);
        eventService.save(new Event(userId, card.getId(), "Оплата штрафов", "", EventType.CARD, -amount, TimestampHelper.now()));
    }

    private void validateCardAndAmount(Card card, int amount) {
        if (card == null) {
            throw new CommonErrorException("Карта не найдена");
        }

        if (amount < 0) {
            throw new CommonErrorException("Неверная сумма");
        }

        if (amount > card.getBalance()) {
            throw new CommonErrorException("Недостаточно средств");
        }
    }
}
