package com.ibank.service;

import com.ibank.entity.Client;
import com.ibank.exception.CommonErrorException;
import com.ibank.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by worker on 10/31/17.
 */
@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public ClientService(ClientRepository clientRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.clientRepository = clientRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Transactional
    public void changeLogin(int userId, String login) {
        Client conflict = clientRepository.findByLogin(login);
        if (conflict != null) {
            throw new CommonErrorException();
        }

        Client client = clientRepository.findById(userId);
        client.setLogin(login);
        clientRepository.save(client);
    }

    @Transactional
    public void changePassword(int userId, String current, String future) {
        Client client = clientRepository.findById(userId);
        current = bCryptPasswordEncoder.encode(current);
        future = bCryptPasswordEncoder.encode(future);

        if (!client.getPassword().equals(current)) {
            throw new CommonErrorException();
        }

        client.setPassword(future);
        clientRepository.save(client);
    }
}
