package com.ibank.service;

import com.ibank.entity.BankDeposit;
import com.ibank.entity.Card;
import com.ibank.entity.Deposit;
import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import com.ibank.exception.CommonErrorException;
import com.ibank.helper.TimestampHelper;
import com.ibank.repository.BankDepositRepository;
import com.ibank.repository.CardRepository;
import com.ibank.repository.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by worker on 11/8/17.
 */
@Service
public class DepositService {
    private final DepositRepository depositRepository;
    private final CardRepository cardRepository;
    private final BankDepositRepository bankDepositRepository;
    private final EventService eventService;

    @Autowired
    public DepositService(
            DepositRepository depositRepository,
            CardRepository cardRepository,
            BankDepositRepository bankDepositRepository,
            EventService eventService
    ) {
        this.depositRepository = depositRepository;
        this.cardRepository = cardRepository;
        this.bankDepositRepository = bankDepositRepository;
        this.eventService = eventService;
    }

    public List<Deposit> findAll(int userId) {
        return depositRepository.findAllByUserId(userId);
    }

    public Deposit findById(int userId, int id) {
        return depositRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void create(int userId, int bankDepositId) {
        BankDeposit bankDeposit = bankDepositRepository.findOne(bankDepositId);
        if (bankDeposit == null) {
            throw new CommonErrorException("Вклад не найден");
        }

        Deposit deposit = depositRepository.save(new Deposit(
                userId,
                bankDeposit.getName(),
                0,
                bankDeposit.getPercent(),
                bankDeposit.getDuration(),
                TimestampHelper.now()
        ));
        eventService.save(new Event(userId, deposit.getId(), "Открытие вклада", "", EventType.DEPOSIT, TimestampHelper.now()));
    }

    @Transactional
    public void refill(int userId, int depositId, int cardId, int amount) {
        Deposit deposit = depositRepository.findByUserIdAndId(userId, depositId);
        if (deposit == null) {
            throw new CommonErrorException("Вклад не найден");
        }

        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        if (card == null) {
            throw new CommonErrorException("Карта не найдена");
        }

        if (amount < 0) {
            throw new CommonErrorException("Неверная сумма");
        }

        if (amount > card.getBalance()) {
            throw new CommonErrorException("Недостаточно средств");
        }

        card.setBalance(card.getBalance() - amount);
        eventService.save(new Event(userId, card.getId(), "Перевод средств", "", EventType.CARD, -amount, TimestampHelper.now()));

        deposit.setBalance(deposit.getBalance() + amount);
        eventService.save(new Event(userId, deposit.getId(), "Пополнение вклада", "", EventType.DEPOSIT, amount, TimestampHelper.now()));
    }

    @Transactional
    public void close(int userId, int depositId, int cardId) {
        Deposit deposit = depositRepository.findByUserIdAndId(userId, depositId);
        if (deposit == null) {
            throw new CommonErrorException("Вклад не найден");
        }

        Card card = cardRepository.findByUserIdAndId(userId, cardId);
        if (card == null) {
            throw new CommonErrorException("Карта не найдена");
        }

        int amount = deposit.getBalance();
        depositRepository.delete(depositId);
        eventService.save(new Event(userId, deposit.getId(), "Закрытие вклада вклада", "", EventType.DEPOSIT, -amount, TimestampHelper.now()));

        card.setBalance(card.getBalance() + amount);
        eventService.save(new Event(userId, card.getId(), "Перевод средств", "", EventType.CARD, amount, TimestampHelper.now()));
    }
}
