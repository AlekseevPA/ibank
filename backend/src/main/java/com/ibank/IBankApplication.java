package com.ibank;

import com.ibank.entity.*;
import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;
import com.ibank.enumeration.EventType;
import com.ibank.helper.TimestampHelper;
import com.ibank.repository.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class IBankApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(IBankApplication.class, args);
		demo(context);
	}

	public static void demo(ApplicationContext context) {
		BankDepositRepository bankDepositRepository = context.getBean(BankDepositRepository.class);
		{
			bankDepositRepository.save(new BankDeposit("Онлайн", 6, 800));
			bankDepositRepository.save(new BankDeposit("Онлайн+", 12, 880));
		}

		for (int i = 0; i < 10; i++) {
			BCryptPasswordEncoder bCryptPasswordEncoder = context.getBean(BCryptPasswordEncoder.class);

			ClientRepository clientRepository = context.getBean(ClientRepository.class);
			Client client = clientRepository.save(new Client("user00" + i, bCryptPasswordEncoder.encode("password" + i), "Пользователь " + i));

			CardRepository cardRepository = context.getBean(CardRepository.class);
			String number = "52" + String.valueOf(System.currentTimeMillis());
			Card card = cardRepository.save(new Card(client.getId(), "Дебетовая карта", number, "*" + number.substring(11), TimestampHelper.fromNow(2, 0, 0, 0, 0, 0), 10_000, CardManufacturer.MASTERCARD, CardType.DEBIT, false));

			DepositRepository depositRepository = context.getBean(DepositRepository.class);
			Deposit deposit = depositRepository.save(new Deposit(client.getId(), "Вклад для активных", 500_000, 880, 12, TimestampHelper.beforeNow(0, 6, 0, 0, 0, 0)));

			EventRepository eventRepository = context.getBean(EventRepository.class);
			{
				Event event = eventRepository.save(new Event(client.getId(), deposit.getId(), "Открытие вклада", "", EventType.DEPOSIT, 500_000, TimestampHelper.beforeNow(0, 6, 0, 0, 0, 0)));
			}
			{
				Event event = eventRepository.save(new Event(client.getId(), card.getId(), "Снятие наличных", "", EventType.CARD, -10_000, TimestampHelper.beforeNow(0, 3, 0, 0, 0, 0)));
			}
			{
				Event event = eventRepository.save(new Event(client.getId(), card.getId(), "Снятие наличных", "", EventType.CARD, -1_000, TimestampHelper.beforeNow(0, 2, 8, 0, 0, 0)));
			}
			{
				Event event = eventRepository.save(new Event(client.getId(), card.getId(), "Пополнение", "", EventType.CARD, 8_000, TimestampHelper.beforeNow(0, 2, 0, 0, 0, 0)));
			}
		}

		// TODO: Payments
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
