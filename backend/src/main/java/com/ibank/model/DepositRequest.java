package com.ibank.model;

import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;

/**
 * Created by worker on 10/31/17.
 */
public class DepositRequest {
    private int bankDepositId;
    private int amount;

    public DepositRequest() {
    }

    public DepositRequest(int bankDepositId) {
        this.bankDepositId = bankDepositId;
    }

    public int getBankDepositId() {
        return bankDepositId;
    }

    public void setBankDepositId(int bankDepositId) {
        this.bankDepositId = bankDepositId;
    }
}
