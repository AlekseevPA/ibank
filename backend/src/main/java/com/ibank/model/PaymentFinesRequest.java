package com.ibank.model;

/**
 * Created by worker on 11/8/17.
 */
public class PaymentFinesRequest {
    private int cardId;
    private String code;
    private int amount;

    public PaymentFinesRequest() {
    }

    public PaymentFinesRequest(int cardId, String code, int amount) {
        this.cardId = cardId;
        this.code = code;
        this.amount = amount;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
