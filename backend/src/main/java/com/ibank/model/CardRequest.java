package com.ibank.model;

import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;

/**
 * Created by worker on 10/31/17.
 */
public class CardRequest {
    private CardManufacturer manufacturer;
    private CardType type;

    public CardRequest() {
    }

    public CardRequest(CardManufacturer manufacturer, CardType type) {
        this.manufacturer = manufacturer;
        this.type = type;
    }

    public CardManufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(CardManufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }
}
