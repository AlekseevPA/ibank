package com.ibank.model;

/**
 * Created by worker on 11/8/17.
 */
public class DepositCloseRequest {
    private int cardId;

    public DepositCloseRequest() {
    }

    public DepositCloseRequest(int cardId) {
        this.cardId = cardId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }
}
