package com.ibank.model;

/**
 * Created by worker on 11/8/17.
 */
public class PaymentMobileRequest {
    private int cardId;
    private String phone;
    private int amount;

    public PaymentMobileRequest() {
    }

    public PaymentMobileRequest(int cardId, String phone, int amount) {
        this.cardId = cardId;
        this.phone = phone;
        this.amount = amount;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
