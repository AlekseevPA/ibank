package com.ibank.model;

/**
 * Created by worker on 11/1/17.
 */
public class LoginRequest {
    private String login;

    public LoginRequest() {
    }

    public LoginRequest(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
