package com.ibank.model;

/**
 * Created by worker on 11/1/17.
 */
public class PasswordRequest {
    private String current;
    private String future;

    public PasswordRequest() {

    }

    public PasswordRequest(String current, String future) {
        this.current = current;
        this.future = future;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getFuture() {
        return future;
    }

    public void setFuture(String future) {
        this.future = future;
    }
}
