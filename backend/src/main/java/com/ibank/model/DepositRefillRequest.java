package com.ibank.model;

/**
 * Created by worker on 11/8/17.
 */
public class DepositRefillRequest {
    private int cardId;
    private int amount;

    public DepositRefillRequest() {
    }

    public DepositRefillRequest(int cardId, int amount) {
        this.cardId = cardId;
        this.amount = amount;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
