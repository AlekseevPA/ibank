package com.ibank.model;

import com.ibank.enumeration.CardManufacturer;
import com.ibank.enumeration.CardType;

/**
 * Created by worker on 10/31/17.
 */
public class TransferRequest {
    private int cardId;
    private String number;
    private int amount;

    public TransferRequest() {
    }

    public TransferRequest(int cardId, String number, int amount) {
        this.cardId = cardId;
        this.number = number;
        this.amount = amount;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
