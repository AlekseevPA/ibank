package com.ibank.model;

/**
 * Created by worker on 11/8/17.
 */
public class PaymentCommunalServicesRequest {
    private int cardId;
    private String org;
    private String account;
    private String value;
    private int amount;

    public PaymentCommunalServicesRequest() {
    }

    public PaymentCommunalServicesRequest(int cardId, String org, String account, String value, int amount) {
        this.cardId = cardId;
        this.org = org;
        this.account = account;
        this.value = value;
        this.amount = amount;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
