package com.ibank.enumeration;

/**
 * Created by worker on 11/1/17.
 */
public enum EventType {
    SYSTEM, CARD, DEPOSIT
}
