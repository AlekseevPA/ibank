package com.ibank.enumeration;

/**
 * Created by worker on 10/31/17.
 */
public enum CardType {
    CREDIT, DEBIT
}
