package com.ibank.repository;

import com.ibank.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by worker on 10/31/17.
 */
@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
    List<Card> findAllByUserId(int userId);
    Card findByUserIdAndId(int userId, int id);
    Card findByNumber(String number);
}
