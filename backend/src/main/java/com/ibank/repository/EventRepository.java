package com.ibank.repository;

import com.ibank.entity.Event;
import com.ibank.enumeration.EventType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by worker on 10/31/17.
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    List<Event> findAllByUserIdOrderByIdDesc(int userId);
    List<Event> findAllByUserIdOrderByIdDesc(int userId, Pageable pageable);
    List<Event> findAllByUserIdAndProductIdAndTypeOrderByIdDesc(int userId, int productId, EventType type);
}
