package com.ibank.repository;

import com.ibank.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by worker on 10/31/17.
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}
