package com.ibank.repository;

import com.ibank.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by worker on 11/8/17.
 */
@Repository
public interface TransferRepository extends JpaRepository<Transfer, Integer> {
}
