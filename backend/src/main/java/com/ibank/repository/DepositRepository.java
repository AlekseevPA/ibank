package com.ibank.repository;

import com.ibank.entity.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by worker on 10/31/17.
 */
@Repository
public interface DepositRepository extends JpaRepository<Deposit, Integer> {
    List<Deposit> findAllByUserId(int userId);
    Deposit findByUserIdAndId(int userId, int id);
}
