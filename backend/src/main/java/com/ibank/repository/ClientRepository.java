package com.ibank.repository;

import com.ibank.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by worker on 10/31/17.
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
    Client findById(int id);
    Client findByLogin(String login);
    Client findByLoginAndPassword(String login, String password);
}
