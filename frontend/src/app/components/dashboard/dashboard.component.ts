import { Component, OnInit } from '@angular/core';
import { Card } from '../../models/card';
import { CardService } from '../../services/card.service';
import { Deposit } from '../../models/deposit';
import { DepositService } from '../../services/deposit.service';
import { Event } from '../../models/event';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public cards: Card[] = [];
  public deposits: Deposit[] = [];
  public events: Event[] = [];

  constructor(
    private cardService: CardService,
    private depositService: DepositService,
    private eventService: EventService
  ) { }

  ngOnInit() {
    this.cardService.getAll().subscribe(data => {
      this.cards = data;
    });

    this.depositService.getAll().subscribe(data => {
      this.deposits = data;
    });

    this.eventService.getLimited(5).subscribe(data => {
      this.events = data;
    });
  }

}
