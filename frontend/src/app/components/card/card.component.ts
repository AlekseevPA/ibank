import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CardService } from '../../services/card.service';
import { Card } from '../../models/card';
import { EventService } from '../../services/event.service';
import { Event } from '../../models/event';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  private id: number;
  private routeSubscribe: any;
  public card: Card = null;
  public events: Event[] = [];

  constructor(
    private cardService: CardService,
    private eventService: EventService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.routeSubscribe = this.activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);

      this.cardService.get(this.id).subscribe(data => {
        this.card = data;
      });

      this.getProductEvents();
    });
  }

  public block() {
    this.cardService.block(this.id).subscribe(data => {
      this.card.blocked = true;

      this.getProductEvents();
    }, err => {
      alert(err.error.message);
    });
  }

  private getProductEvents() {
    this.eventService.getForProduct(this.id, 'CARD').subscribe(data => {
      this.events = data;
    });
  }

}
