import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public auth = {login: '', currentPassword: '', futurePassword: '', confirmationPassword: ''};
  public status = '';
  public message = '';

  constructor(private clientService: ClientService) { }

  ngOnInit() {
  }

  public changeLogin() {
    this.clientService.changeLogin(this.auth.login).subscribe(data => {
      this.status = 'success';
      this.message = 'Логин успешно изменён';
    }, err => {
      this.status = 'fail';
      this.message = 'Не удалось изменить логин';
    });
  }

  public changePassword() {
    this.clientService.changePassword(this.auth.currentPassword, this.auth.futurePassword).subscribe(data => {
      this.status = 'success';
      this.message = 'Пароль успешно изменён';
    }, err => {
      this.status = 'fail';
      this.message = 'Не удалось изменить пароль';
    });
  }

}
