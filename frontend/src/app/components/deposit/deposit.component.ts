import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DepositService } from '../../services/deposit.service';
import { Deposit } from '../../models/deposit';
import { EventService } from '../../services/event.service';
import { Event } from '../../models/event';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent implements OnInit {
  public id: number;
  private routeSubscribe: any;
  public deposit: Deposit = null;
  public events: Event[] = [];

  constructor(
    private depositService: DepositService,
    private eventService: EventService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.routeSubscribe = this.activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);

      this.depositService.get(this.id).subscribe(data => {
        this.deposit = data;
      });

      this.getProductEvents();
    });
  }

  private getProductEvents() {
    this.eventService.getForProduct(this.id, 'DEPOSIT').subscribe(data => {
      this.events = data;
    });
  }
}
