import { Component, OnInit } from '@angular/core';
import { TransferRequest } from '../../models/transfer-request';
import { TransferService } from '../../services/transfer.service';
import { Card } from '../../models/card';
import { CardService } from '../../services/card.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transfer-request',
  templateUrl: './transfer-request.component.html',
  styleUrls: ['./transfer-request.component.scss']
})
export class TransferRequestComponent implements OnInit {
  public cards: Card[] = [];
  public data: TransferRequest = { cardId: 0, number: '', amount: 0 };

  constructor(
    private transferService: TransferService,
    private cardService: CardService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.transferService.create(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }
}
