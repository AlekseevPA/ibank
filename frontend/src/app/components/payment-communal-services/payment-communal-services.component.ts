import { Component, OnInit } from '@angular/core';
import { CardService } from '../../services/card.service';
import { PaymentService } from '../../services/payment.service';
import { Router } from '@angular/router';
import { Card } from '../../models/card';
import { PaymentCommunalServicesRequest } from '../../models/payment-communal-services-request';

@Component({
  selector: 'app-payment-communal-services',
  templateUrl: './payment-communal-services.component.html',
  styleUrls: ['./payment-communal-services.component.scss']
})
export class PaymentCommunalServicesComponent implements OnInit {
  public cards: Card[] = [];
  public data: PaymentCommunalServicesRequest = { cardId: 0, org: '', account: '', value: '', amount: 0 };

  constructor(
    private paymentService: PaymentService,
    private cardService: CardService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.paymentService.createCommunalServicesPayment(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }

}
