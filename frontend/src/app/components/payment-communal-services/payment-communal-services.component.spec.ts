import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCommunalServicesComponent } from './payment-communal-services.component';

describe('PaymentCommunalServicesComponent', () => {
  let component: PaymentCommunalServicesComponent;
  let fixture: ComponentFixture<PaymentCommunalServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentCommunalServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCommunalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
