import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AuthRequest } from '../../models/auth-request';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  public auth: AuthRequest = {login: '', password: ''};

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/cabinet']);
      return;
    }
  }

  public authenticate() {
    this.authService.authenticate(this.auth).subscribe(data => {
      this.authService.save(String(data.body));
      this.router.navigate(['/cabinet']);
    }, err => {
      alert(err.error.message);
    });
  }

}
