import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../../services/payment.service';
import { CardService } from '../../services/card.service';
import { Router } from '@angular/router';
import { Card } from '../../models/card';
import { PaymentMobileRequest } from '../../models/payment-mobile-request';

@Component({
  selector: 'app-payment-mobile',
  templateUrl: './payment-mobile.component.html',
  styleUrls: ['./payment-mobile.component.scss']
})
export class PaymentMobileComponent implements OnInit {
  public cards: Card[] = [];
  public data: PaymentMobileRequest = { cardId: 0, phone: '', amount: 0 };

  constructor(
    private paymentService: PaymentService,
    private cardService: CardService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.paymentService.createMobilePayment(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }

}
