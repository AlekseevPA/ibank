import { Component, OnInit } from '@angular/core';
import { Event } from '../../models/event';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {
  public events: Event[] = [];
  public page = 1;
  public done = false;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.load();
  }

  public load() {
    this.eventService.getPage(this.page).subscribe(data => {
      if (data.length === 0) {
        this.done = true;
        return;
      }
      this.events = [...this.events, ...data];
      this.page++;
    });
  }

}
