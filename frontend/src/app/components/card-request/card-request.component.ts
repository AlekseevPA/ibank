import { Component, OnInit } from '@angular/core';
import { CardRequest } from '../../models/card-request';
import { CardService } from '../../services/card.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-request',
  templateUrl: './card-request.component.html',
  styleUrls: ['./card-request.component.scss']
})
export class CardRequestComponent implements OnInit {
  public manufacturers = [{value: 'MASTERCARD', title: 'MasterCard'}, {value: 'VISA', title: 'Visa'}];
  public types = [{value: 'DEBIT', title: 'Дебетовая карта'}, {value: 'CREDIT', title: 'Кредитная карта'}];
  public data: CardRequest = {manufacturer: '', type: ''};

  constructor(
    private cardService: CardService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public request() {
    this.cardService.create(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }
}
