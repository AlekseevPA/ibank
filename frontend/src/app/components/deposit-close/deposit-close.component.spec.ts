import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositCloseComponent } from './deposit-close.component';

describe('DepositCloseComponent', () => {
  let component: DepositCloseComponent;
  let fixture: ComponentFixture<DepositCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
