import { Component, OnInit } from '@angular/core';
import { Card } from '../../models/card';
import { DepositCloseRequest } from '../../models/deposit-close-request';
import { DepositService } from '../../services/deposit.service';
import { CardService } from '../../services/card.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-deposit-close',
  templateUrl: './deposit-close.component.html',
  styleUrls: ['./deposit-close.component.scss']
})
export class DepositCloseComponent implements OnInit {
  private id: number;
  private routeSubscribe: any;
  public cards: Card[] = [];
  public data: DepositCloseRequest = { cardId: 0 };

  constructor(
    private depositService: DepositService,
    private cardService: CardService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.routeSubscribe = this.activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);
    });

    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.depositService.close(this.id, this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }
}
