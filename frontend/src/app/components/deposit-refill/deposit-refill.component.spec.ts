import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositRefillComponent } from './deposit-refill.component';

describe('DepositRefillComponent', () => {
  let component: DepositRefillComponent;
  let fixture: ComponentFixture<DepositRefillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositRefillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositRefillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
