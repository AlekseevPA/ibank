import { Component, OnInit } from '@angular/core';
import { Card } from '../../models/card';
import { DepositRefillRequest } from '../../models/deposit-refill-request';
import { DepositService } from '../../services/deposit.service';
import { CardService } from '../../services/card.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-deposit-refill',
  templateUrl: './deposit-refill.component.html',
  styleUrls: ['./deposit-refill.component.scss']
})
export class DepositRefillComponent implements OnInit {
  private id: number;
  private routeSubscribe: any;
  public cards: Card[] = [];
  public data: DepositRefillRequest = { cardId: 0, amount: 0};

  constructor(
    private depositService: DepositService,
    private cardService: CardService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.routeSubscribe = this.activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);
    });

    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.depositService.refill(this.id, this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }

}
