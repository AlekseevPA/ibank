import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFinesComponent } from './payment-fines.component';

describe('PaymentFinesComponent', () => {
  let component: PaymentFinesComponent;
  let fixture: ComponentFixture<PaymentFinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentFinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
