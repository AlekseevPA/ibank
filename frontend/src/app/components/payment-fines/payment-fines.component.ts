import { Component, OnInit } from '@angular/core';
import { Card } from '../../models/card';
import { PaymentFinesRequest } from '../../models/payment-fines-request';
import { PaymentService } from '../../services/payment.service';
import { CardService } from '../../services/card.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-fines',
  templateUrl: './payment-fines.component.html',
  styleUrls: ['./payment-fines.component.scss']
})
export class PaymentFinesComponent implements OnInit {
  public cards: Card[] = [];
  public data: PaymentFinesRequest = { cardId: 0, code: '', amount: 0 };

  constructor(
    private paymentService: PaymentService,
    private cardService: CardService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cardService.getAll().subscribe(data => {
      this.cards = data.filter(item => !item.blocked);
    });
  }

  public request() {
    this.paymentService.createFinesPayment(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }
}
