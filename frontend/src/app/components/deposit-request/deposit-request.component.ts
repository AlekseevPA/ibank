import { Component, OnInit } from '@angular/core';
import { BankDepositService } from '../../services/bank-deposit.service';
import { BankDeposit } from '../../models/bank-deposit';
import { DepositRequest } from '../../models/deposit-request';
import { DepositService } from '../../services/deposit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deposit-request',
  templateUrl: './deposit-request.component.html',
  styleUrls: ['./deposit-request.component.scss']
})
export class DepositRequestComponent implements OnInit {
  public bankDeposits: BankDeposit[] = [];
  public data: DepositRequest = {bankDepositId: 0};

  constructor(
    private bankDepositService: BankDepositService,
    private depositService: DepositService,
    private router: Router
  ) { }

  ngOnInit() {
    this.bankDepositService.getAll().subscribe(data => {
      this.bankDeposits = data;
    });
  }

  public request() {
    this.depositService.create(this.data).subscribe(data => {
      this.router.navigateByUrl('/');
    }, err => {
      alert(err.error.message);
    });
  }

}
