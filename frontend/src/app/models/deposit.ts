import { Product } from './product';

export interface Deposit extends Product {
  balance: number;
  percent: number;
  duration: number;
  expire: number;
  timestamp: number;
}
