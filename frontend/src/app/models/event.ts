export interface Event {
  id: number;
  userId: number;
  productId: number;
  name: string;
  description: string;
  type: string;
  amount: string;
  timestamp: number;
}
