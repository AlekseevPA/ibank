export interface BankDeposit {
  id: number;
  name: string;
  duration: number;
  percent: number;
}
