export interface DepositRefillRequest {
  cardId: number;
  amount: number;
}
