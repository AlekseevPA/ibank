export interface PaymentCommunalServicesRequest {
  cardId: number;
  org: string;
  account: string;
  value: string;
  amount: number;
}
