export interface TransferRequest {
  cardId: number;
  number: string;
  amount: number;
}
