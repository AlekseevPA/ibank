export interface PaymentMobileRequest {
  cardId: number;
  phone: string;
  amount: number;
}
