export interface PaymentFinesRequest {
  cardId: number;
  code: string;
  amount: number;
}
