import { Product } from './product';

export interface Card extends Product {
  number: string;
  masked: string,
  expire: number;
  balance: number;
  manufacturer: string; // TODO: Make enum
  type: string; // TODO: make enum
  blocked: boolean;
}
