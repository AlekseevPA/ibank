export interface CardRequest {
  manufacturer: string;
  type: string;
}
