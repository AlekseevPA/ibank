import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CardComponent } from './components/card/card.component';
import { DepositComponent } from './components/deposit/deposit.component';
import { ClientComponent } from './components/client/client.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { CabinetComponent } from './components/cabinet/cabinet.component';
import { AuthService } from './services/auth.service';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { CardService } from './services/card.service';
import { DepositService } from './services/deposit.service';
import { EventService } from './services/event.service';
import { CardRequestComponent } from './components/card-request/card-request.component';
import { PaymentsListComponent } from './components/payments-list/payments-list.component';
import { EventItemComponent } from './components/event-item/event-item.component';
import { ClientService } from './services/client.service';
import { EventsListComponent } from './components/events-list/events-list.component';
import { DepositRequestComponent } from './components/deposit-request/deposit-request.component';
import { BankDepositService } from './services/bank-deposit.service';
import { TransferService } from './services/transfer.service';
import { TransferRequestComponent } from './components/transfer-request/transfer-request.component';
import { AmountNotEnoughDirective } from './directives/amount-not-enough.directive';
import { DepositRefillComponent } from './components/deposit-refill/deposit-refill.component';
import { DepositCloseComponent } from './components/deposit-close/deposit-close.component';
import { PaymentMobileComponent } from './components/payment-mobile/payment-mobile.component';
import { PaymentCommunalServicesComponent } from './components/payment-communal-services/payment-communal-services.component';
import { PaymentFinesComponent } from './components/payment-fines/payment-fines.component';
import { PaymentService } from './services/payment.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    DashboardComponent,
    CardComponent,
    DepositComponent,
    ClientComponent,
    ToolbarComponent,
    CabinetComponent,
    CardRequestComponent,
    PaymentsListComponent,
    EventItemComponent,
    EventsListComponent,
    DepositRequestComponent,
    TransferRequestComponent,
    AmountNotEnoughDirective,
    DepositRefillComponent,
    DepositCloseComponent,
    PaymentMobileComponent,
    PaymentCommunalServicesComponent,
    PaymentFinesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    CardService,
    DepositService,
    BankDepositService,
    EventService,
    ClientService,
    TransferService,
    PaymentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
