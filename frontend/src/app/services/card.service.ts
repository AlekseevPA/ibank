import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Card } from '../models/card';
import { Observable } from 'rxjs/Observable';
import { CardRequest } from '../models/card-request';
import { environment } from '../../environments/environment';

@Injectable()
export class CardService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Card[]> {
    return this.http.get<Card[]>(environment.apiURL + '/cards');
  }

  public get(id: number): Observable<Card> {
    return this.http.get<Card>(environment.apiURL + '/cards/' + id);
  }

  public block(id: number): Observable<Object> {
    return this.http.delete(environment.apiURL + '/cards/' + id, { responseType: 'text', observe: 'response' });
  }

  public create(request: CardRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/cards', request, { responseType: 'text', observe: 'response' });
  }
}
