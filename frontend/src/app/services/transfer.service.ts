import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TransferRequest } from '../models/transfer-request';
import { environment } from '../../environments/environment';

@Injectable()
export class TransferService {

  constructor(private http: HttpClient) { }

  public create(request: TransferRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/transfers', request,
      {
        responseType: 'text',
        observe: 'response'
      });
  }
}
