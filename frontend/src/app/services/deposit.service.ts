import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Deposit } from '../models/deposit';
import { Observable } from 'rxjs/Observable';
import { DepositRequest } from '../models/deposit-request';
import { environment } from '../../environments/environment';
import { DepositRefillRequest } from '../models/deposit-refill-request';
import { DepositCloseRequest } from '../models/deposit-close-request';

@Injectable()
export class DepositService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Deposit[]> {
    return this.http.get<Deposit[]>(environment.apiURL + '/deposits');
  }

  public get(id: number): Observable<Deposit> {
    return this.http.get<Deposit>(environment.apiURL + '/deposits/' + id);
  }

  public create(request: DepositRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/deposits', request, { responseType: 'text', observe: 'response' });
  }

  public refill(id: number, request: DepositRefillRequest): Observable<Object> {
    return this.http.put(environment.apiURL + '/deposits/' + id + '/refill',
      request,
      { responseType: 'text', observe: 'response' }
    );
  }

  public close(id: number, request: DepositCloseRequest): Observable<Object> {
    return this.http.put(environment.apiURL + '/deposits/' + id + '/close',
      request,
      { responseType: 'text', observe: 'response' }
    );
  }

}
