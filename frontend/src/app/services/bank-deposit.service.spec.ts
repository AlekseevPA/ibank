import { TestBed, inject } from '@angular/core/testing';

import { BankDepositService } from './bank-deposit.service';

describe('BankDepositService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BankDepositService]
    });
  });

  it('should be created', inject([BankDepositService], (service: BankDepositService) => {
    expect(service).toBeTruthy();
  }));
});
