import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class ClientService {

  constructor(private http: HttpClient) { }

  public changeLogin(login: string): Observable<Object> {
    return this.http.put(environment.apiURL + '/clients/login',
      { login: login },
      { responseType: 'text', observe: 'response' }
    );
  }

  public changePassword(current: string, future: string): Observable<Object> {
    return this.http.put(environment.apiURL + '/clients/password',
      { current: current, future: future},
      { responseType: 'text', observe: 'response' }
    );
  }

}
