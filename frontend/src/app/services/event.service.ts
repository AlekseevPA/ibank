import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Event } from '../models/event';
import { environment } from '../../environments/environment';

@Injectable()
export class EventService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Event[]> {
    return this.http.get<Event[]>(environment.apiURL + '/events');
  }

  public getLimited(limit: number): Observable<Event[]> {
    const params = new HttpParams().set('count', String(limit));
    return this.http.get<Event[]>(environment.apiURL + '/events', {params: params});
  }

  public getPage(page: number): Observable<Event[]> {
    const params = new HttpParams().set('page', String(page));
    return this.http.get<Event[]>(environment.apiURL + '/events', {params: params});
  }

  public getForProduct(id: number, type: string): Observable<Event[]> {
    return this.http.get<Event[]>(environment.apiURL + '/events/product/' + type + '/' + id);
  }
}
