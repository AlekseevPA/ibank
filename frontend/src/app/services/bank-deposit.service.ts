import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BankDeposit } from '../models/bank-deposit';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class BankDepositService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<BankDeposit[]> {
    return this.http.get<BankDeposit[]>(environment.apiURL + '/bank/deposits');
  }
}
