import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentMobileRequest } from '../models/payment-mobile-request';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { PaymentCommunalServicesRequest } from '../models/payment-communal-services-request';
import { PaymentFinesRequest } from '../models/payment-fines-request';

@Injectable()
export class PaymentService {

  constructor(private http: HttpClient) { }

  public createMobilePayment(request: PaymentMobileRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/payments/mobile', request,
      {
        responseType: 'text',
        observe: 'response'
      });
  }

  public createCommunalServicesPayment(request: PaymentCommunalServicesRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/payments/communal-services', request,
      {
        responseType: 'text',
        observe: 'response'
      });
  }

  public createFinesPayment(request: PaymentFinesRequest): Observable<Object> {
    return this.http.post(environment.apiURL + '/payments/fines', request,
      {
        responseType: 'text',
        observe: 'response'
      });
  }
}
