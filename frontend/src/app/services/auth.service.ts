import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { Client } from '../models/client';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }

  public isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  public authenticate(auth): Observable<HttpResponse<string>> {
    return this.http.post(environment.apiURL + '/../login', auth,
      {
        responseType: 'text',
        observe: 'response'
      });
  }

  public save(token: string): void {
    localStorage.setItem('token', token);
  }

  public remove(): void {
    localStorage.removeItem('token');
  }

}
