import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function amountNotEnoughValidator(balance: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    const result = balance >= Number(control.value);
    return result ? {'amountNotEnough': {value: control.value}} : null;
  };
}
