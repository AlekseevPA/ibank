import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { amountNotEnoughValidator } from '../validators/amount-not-enough.validator';

@Directive({
  selector: '[appAmountNotEnough]',
  providers: [{provide: NG_VALIDATORS, useExisting: AmountNotEnoughDirective, multi: true}]
})
export class AmountNotEnoughDirective implements Validator {
  @Input() balance: number;

  validate(control: AbstractControl): ValidationErrors {
    return amountNotEnoughValidator(this.balance)(control);
  }
}
