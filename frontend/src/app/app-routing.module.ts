import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CardComponent } from './components/card/card.component';
import { DepositComponent } from './components/deposit/deposit.component';
import { ClientComponent } from './components/client/client.component';
import { CabinetComponent } from './components/cabinet/cabinet.component';
import { CardRequestComponent } from './components/card-request/card-request.component';
import { EventsListComponent } from './components/events-list/events-list.component';
import { DepositRequestComponent } from './components/deposit-request/deposit-request.component';
import { TransferRequestComponent } from './components/transfer-request/transfer-request.component';
import { DepositRefillComponent } from './components/deposit-refill/deposit-refill.component';
import { DepositCloseComponent } from './components/deposit-close/deposit-close.component';
import { PaymentsListComponent } from './components/payments-list/payments-list.component';
import { PaymentMobileComponent } from './components/payment-mobile/payment-mobile.component';
import { PaymentCommunalServicesComponent } from './components/payment-communal-services/payment-communal-services.component';
import { PaymentFinesComponent } from './components/payment-fines/payment-fines.component';

const routes: Routes = [
  { path: '', component: AppComponent, children: [
    { path: 'auth', component: AuthComponent },
    { path: 'cabinet', component: CabinetComponent, children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'cards/:id', component: CardComponent },
      { path: 'card-request', component: CardRequestComponent },
      { path: 'deposits/:id', component: DepositComponent },
      { path: 'deposits/:id/refill', component: DepositRefillComponent },
      { path: 'deposits/:id/close', component: DepositCloseComponent },
      { path: 'deposit-request', component: DepositRequestComponent },
      { path: 'transfer-request', component: TransferRequestComponent },
      { path: 'payments', component: PaymentsListComponent },
      { path: 'payments/mobile', component: PaymentMobileComponent },
      { path: 'payments/communal-services', component: PaymentCommunalServicesComponent },
      { path: 'payments/fines', component: PaymentFinesComponent },
      { path: 'events', component: EventsListComponent },
      { path: 'client', component: ClientComponent },
      { path: '**', redirectTo: 'dashboard' }
    ]},
    { path: '**', redirectTo: 'auth' }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
