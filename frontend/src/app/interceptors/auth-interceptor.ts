import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (localStorage.getItem('token') !== null) {
      const token = localStorage.getItem('token');
      return next.handle(req.clone( {
        setHeaders: {
          'Authorization': 'Bearer ' + token
        }
      }));
    }
    return next.handle(req).catch((err: HttpErrorResponse) => {
      if (err.error instanceof Error) {
        // TODO:
      } else {
        if (err.status === 401 || err.status === 403) {
          this.router.navigate(['/auth']);
          return Observable.throw({error: {message: 'Необходима авторизация'}});
        }
      }

      return Observable.throw({error: {message: 'Произошла ошибка, попробуйте повторить ваш запрос позже'}});
    });
  }
}
